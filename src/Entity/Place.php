<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
*/
class Place
{
  /**
  * @ORM\Id()
  * @ORM\GeneratedValue()
  * @ORM\Column(type="integer")
  */
  private $id;

  /**
  * @ORM\Column(type="string", length=255)
  */
  private $name;

  /**
  * @ORM\Column(type="string", length=255)
  */
  private $address;

  /**
  * @ORM\Column(type="string", length=255)
  */
  private $postal_code;

  /**
  * @ORM\Column(type="string", length=255)
  */
  private $city;

  /**
  * @ORM\Column(type="integer", nullable=true)
  */
  private $latitude;

  /**
  * @ORM\Column(type="integer", nullable=true)
  */
  private $longitude;

  /**
  * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="places")
  */
  private $user;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="places")
   */
  private $category;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function setId(int $id): self
  {
    $this->id = $id;

    return $this;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getAddress(): ?string
  {
    return $this->address;
  }

  public function setAddress(string $address): self
  {
    $this->address = $address;

    return $this;
  }

  public function getPostalCode(): ?string
  {
    return $this->postal_code;
  }

  public function setPostalCode(string $postal_code): self
  {
    $this->postal_code = $postal_code;

    return $this;
  }

  public function getCity(): ?string
  {
    return $this->city;
  }

  public function setCity(string $city): self
  {
    $this->city = $city;

    return $this;
  }

  public function getLatitude(): ?int
  {
    return $this->latitude;
  }

  public function setLatitude(?int $latitude): self
  {
    $this->latitude = $latitude;

    return $this;
  }

  public function getLongitude(): ?int
  {
    return $this->longitude;
  }

  public function setLongitude(?int $longitude): self
  {
    $this->longitude = $longitude;

    return $this;
  }

  public function getUserId(): ?User
  {
    return $this->user;
  }

  public function setUserId(?User $user): self
  {
    $this->user = $user;

    return $this;
  }

  public function getCategory(): ?Category
  {
      return $this->category;
  }

  public function setCategory(?Category $category): self
  {
      $this->category = $category;

      return $this;
  }
}
