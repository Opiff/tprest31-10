<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Category;
use App\Entity\Place;

class PlaceController extends AbstractController
{
  /**
  * @Route("/places", name="places", methods="GET")
  */
  public function index(): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $places = $em->getRepository(Place::class)->findAll();

    $data = [];
    foreach ($places as $place) {
      array_push($data, [
        'id' => $place->getId(),
        'category_id' => $place->getCategory()->getId(),
        'name' => $place->getName(),
        'address' => $place->getAddress(),
        'postal_code' => $place->getPostalCode(),
        'city' => $place->getCity(),
        'latitude' => $place->getLatitude(),
        'longitude' => $place->getLongitude()
      ]);
    }

    $code = JsonResponse::HTTP_OK;
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/places/{id}", name="places_id", methods="GET")
  */
  public function places_id($id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $places = $em->getRepository(Place::class)->findAll();

    $data = [];
    foreach ($places as $place) {
      if($place->getId() == $id)
      {
        array_push($data, [
          'id' => $place->getId(),
          'category_id' => $place->getCategory()->getId(),
          'name' => $place->getName(),
          'address' => $place->getAddress(),
          'postal_code' => $place->getPostalCode(),
          'city' => $place->getCity(),
          'latitude' => $place->getLatitude(),
          'longitude' => $place->getLongitude()
        ]);
        $code = JsonResponse::HTTP_OK;
      }
    }

    if ($data == [])
    {
      $code = JsonResponse::HTTP_NOT_FOUND;
    }

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }


  /**
  * @Route("/places/{latitude}/{longitude}/{range}", name="places_localisation", methods="GET")
  */
  public function places_localisation($latitude,$longitude,$range): JsonResponse
  {
    $placeController = new PlaceController();

    $myLat = 10;
    $myLong = 12;
    $dist = $placeController->distance($latitude,$longitude,$myLat,$myLong);
    echo($dist);
    $data = [];

    array_push($data, [
      'dist' => $dist
    ]);

    $em = $this->getDoctrine()->getManager();
    $places = $em->getRepository(Place::class)->findAll();

    $data = [];
    foreach ($places as $place) {
      array_push($data, [
        'id' => $place->getId(),
        'category_id' => $place->getCategory()->getId(),
        'name' => $place->getName(),
        'address' => $place->getAddress(),
        'postal_code' => $place->getPostalCode(),
        'city' => $place->getCity(),
        'latitude' => $place->getLatitude(),
        'longitude' => $place->getLongitude()
      ]);
    }

    $code = JsonResponse::HTTP_OK;
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/places/category/{id}", name="places_category_id", methods="GET")
  */
  public function places_category_id($id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $places = $em->getRepository(Place::class)->findAll();

    $data = [];
    foreach ($places as $place) {
      if($place->getCategory()->getId() == $id)
      {
        array_push($data, [
          'id' => $place->getId(),
          'category_id' => $place->getCategory()->getId(),
          'name' => $place->getName(),
          'address' => $place->getAddress(),
          'postal_code' => $place->getPostalCode(),
          'city' => $place->getCity(),
          'latitude' => $place->getLatitude(),
          'longitude' => $place->getLongitude()
        ]);
        $code = JsonResponse::HTTP_OK;
      }
    }

    if ($data == [])
    {
      $code = JsonResponse::HTTP_NOT_FOUND;
    }

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/places", name="places_create", methods="POST")
  */
  public function places_create(Request $request): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();

    $input = json_decode($request->getContent(), true);


    $category = new Category();
    $category = $em->getRepository(Category::class)->findOneBy(['id' => $input['category_id']]);

    $place = new Place();
    $place->setName($input['name']);
    $place->setAddress($input['address']);
    $place->setPostalCode($input['postal_code']);
    $place->setCity($input['city']);
    $place->setLatitude($input['latitude']);
    $place->setLongitude($input['longitude']);

    $place->setCategory($category);
    $em->persist($place);
    $em->flush();

    $data = [
      'id' => $place->getId(),
      'category_id' => $place->getCategory()->getId(),
      'name' => $place->getName(),
      'address' => $place->getAddress(),
      'postal_code' => $place->getPostalCode(),
      'city' => $place->getCity(),
      'latitude' => $place->getLatitude(),
      'longitude' => $place->getLongitude()
    ];
    $code = JsonResponse::HTTP_CREATED;

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/places/{id}", name="places_delete", methods="DELETE")
  */
  public function places_delete($id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();

    $place = new Place();
    $place = $em->getRepository(Place::class)->findOneBy(['id' => $id]);

    if (!is_null($place)){
      $em->remove($place);
      $em->flush();
      $data = [];
      $code = JsonResponse::HTTP_NO_CONTENT;
    }

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/places/{id}", name="places_modif", methods="PATCH")
  */
  public function places_modif(Request $request, $id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $input = json_decode($request->getContent(), true);

    $category = new Category();
    $category = $em->getRepository(Category::class)->findOneBy(['id' => $input['category_id']]);

    $place = new Place();
    $place = $em->getRepository(Place::class)->findOneBy(['id' => $id]);
    $place->setName($input['name']);
    $place->setAddress($input['address']);
    $place->setPostalCode($input['postal_code']);
    $place->setCity($input['city']);
    $place->setLatitude($input['latitude']);
    $place->setLongitude($input['longitude']);

    $place->setCategory($category);
    $em->persist($place);
    $em->flush();

    $data = [
      'id' => $place->getId(),
      'category_id' => $place->getCategory()->getId(),
      'name' => $place->getName(),
      'address' => $place->getAddress(),
      'postal_code' => $place->getPostalCode(),
      'city' => $place->getCity(),
      'latitude' => $place->getLatitude(),
      'longitude' => $place->getLongitude()
    ];

    $code = JsonResponse::HTTP_CREATED;

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  public function distance($lat1, $lng1, $lat2, $lng2) {
    $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
    $rlo1 = deg2rad($lng1);
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lng2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
    $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
    //
    $meter = ($earth_radius * $d);

    return $meter / 1000;

  }
}
