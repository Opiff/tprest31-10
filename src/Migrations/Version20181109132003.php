<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181109132003 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1A76ED395');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1A76ED395 FOREIGN KEY (user_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE place DROP INDEX IDX_741D53CDA76ED395, ADD UNIQUE INDEX UNIQ_741D53CDA76ED395 (user_id)');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CD12469DE2');
        $this->addSql('DROP INDEX IDX_741D53CD12469DE2 ON place');
        $this->addSql('ALTER TABLE place DROP category_id, CHANGE postal_code postal_code INT NOT NULL, CHANGE latitude latitude INT DEFAULT NULL, CHANGE longitude longitude INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(255) NOT NULL, CHANGE apikey apikey VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1A76ED395');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE place DROP INDEX UNIQ_741D53CDA76ED395, ADD INDEX IDX_741D53CDA76ED395 (user_id)');
        $this->addSql('ALTER TABLE place ADD category_id INT DEFAULT NULL, CHANGE postal_code postal_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE latitude latitude DOUBLE PRECISION DEFAULT NULL, CHANGE longitude longitude DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_741D53CD12469DE2 ON place (category_id)');
        $this->addSql('ALTER TABLE user CHANGE email email LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE apikey apikey LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
