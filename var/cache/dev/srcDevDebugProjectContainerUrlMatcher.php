<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = $allowSchemes = array();
        if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
            return $ret;
        }
        if ($allow) {
            throw new MethodNotAllowedException(array_keys($allow));
        }
        if (!in_array($this->context->getMethod(), array('HEAD', 'GET'), true)) {
            // no-op
        } elseif ($allowSchemes) {
            redirect_scheme:
            $scheme = $this->context->getScheme();
            $this->context->setScheme(key($allowSchemes));
            try {
                if ($ret = $this->doMatch($pathinfo)) {
                    return $this->redirect($pathinfo, $ret['_route'], $this->context->getScheme()) + $ret;
                }
            } finally {
                $this->context->setScheme($scheme);
            }
        } elseif ('/' !== $pathinfo) {
            $pathinfo = '/' !== $pathinfo[-1] ? $pathinfo.'/' : substr($pathinfo, 0, -1);
            if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
                return $this->redirect($pathinfo, $ret['_route']) + $ret;
            }
            if ($allowSchemes) {
                goto redirect_scheme;
            }
        }

        throw new ResourceNotFoundException();
    }

    private function doMatch(string $rawPathinfo, array &$allow = array(), array &$allowSchemes = array()): ?array
    {
        $allow = $allowSchemes = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        switch ($pathinfo) {
            case '/category':
                // category
                $ret = array('_route' => 'category', '_controller' => 'App\\Controller\\CategoryController::index');
                if (!isset(($a = array('GET' => 0))[$canonicalMethod])) {
                    $allow += $a;
                    goto not_category;
                }
    
                return $ret;
                not_category:
                // category_create
                $ret = array('_route' => 'category_create', '_controller' => 'App\\Controller\\CategoryController::category_create');
                if (!isset(($a = array('POST' => 0))[$requestMethod])) {
                    $allow += $a;
                    goto not_category_create;
                }
    
                return $ret;
                not_category_create:
                break;
            case '/places':
                // places
                $ret = array('_route' => 'places', '_controller' => 'App\\Controller\\PlaceController::index');
                if (!isset(($a = array('GET' => 0))[$canonicalMethod])) {
                    $allow += $a;
                    goto not_places;
                }
    
                return $ret;
                not_places:
                // places_create
                $ret = array('_route' => 'places_create', '_controller' => 'App\\Controller\\PlaceController::places_create');
                if (!isset(($a = array('POST' => 0))[$requestMethod])) {
                    $allow += $a;
                    goto not_places_create;
                }
    
                return $ret;
                not_places_create:
                break;
        }

        $matchedPathinfo = $pathinfo;
        $regexList = array(
            0 => '{^(?'
                    .'|/category/([^/]++)(?'
                        .'|(*:28)'
                    .')'
                    .'|/places/(?'
                        .'|([^/]++)(?'
                            .'|(*:58)'
                            .'|/([^/]++)/([^/]++)(*:83)'
                        .')'
                        .'|category/([^/]++)(*:108)'
                        .'|([^/]++)(?'
                            .'|(*:127)'
                        .')'
                    .')'
                .')$}sD',
        );

        foreach ($regexList as $offset => $regex) {
            while (preg_match($regex, $matchedPathinfo, $matches)) {
                switch ($m = (int) $matches['MARK']) {
                    case 28:
                        $matches = array('id' => $matches[1] ?? null);

                        // category_id
                        $ret = $this->mergeDefaults(array('_route' => 'category_id') + $matches, array('_controller' => 'App\\Controller\\CategoryController::category_id'));
                        if (!isset(($a = array('GET' => 0))[$canonicalMethod])) {
                            $allow += $a;
                            goto not_category_id;
                        }
            
                        return $ret;
                        not_category_id:
            
                         // category_modif
                        $ret = $this->mergeDefaults(array('_route' => 'category_modif') + $matches, array('_controller' => 'App\\Controller\\CategoryController::category_modif'));
                        if (!isset(($a = array('PUT' => 0))[$requestMethod])) {
                            $allow += $a;
                            goto not_category_modif;
                        }
            
                        return $ret;
                        not_category_modif:

                        // category_delete
                        $ret = $this->mergeDefaults(array('_route' => 'category_delete') + $matches, array('_controller' => 'App\\Controller\\CategoryController::category_delete'));
                        if (!isset(($a = array('DELETE' => 0))[$requestMethod])) {
                            $allow += $a;
                            goto not_category_delete;
                        }
            
                        return $ret;
                        not_category_delete:

                       break;
                    case 127:
                        $matches = array('id' => $matches[1] ?? null);

                        // places_delete
                        $ret = $this->mergeDefaults(array('_route' => 'places_delete') + $matches, array('_controller' => 'App\\Controller\\PlaceController::places_delete'));
                        if (!isset(($a = array('DELETE' => 0))[$requestMethod])) {
                            $allow += $a;
                            goto not_places_delete;
                        }
            
                        return $ret;
                        not_places_delete:
            
                         // places_modif
                        $ret = $this->mergeDefaults(array('_route' => 'places_modif') + $matches, array('_controller' => 'App\\Controller\\PlaceController::places_modif'));
                        if (!isset(($a = array('PATCH' => 0))[$requestMethod])) {
                            $allow += $a;
                            goto not_places_modif;
                        }
            
                        return $ret;
                        not_places_modif:

                       break;
                    default:
                        $routes = array(
                            58 => array(array('_route' => 'places_id', '_controller' => 'App\\Controller\\PlaceController::places_id'), array('id'), array('GET' => 0), null),
                            83 => array(array('_route' => 'places_localisation', '_controller' => 'App\\Controller\\PlaceController::places_localisation'), array('latitude', 'longitude', 'range'), array('GET' => 0), null),
                            108 => array(array('_route' => 'places_category_id', '_controller' => 'App\\Controller\\PlaceController::places_category_id'), array('id'), array('GET' => 0), null),
                        );
            
                        list($ret, $vars, $requiredMethods, $requiredSchemes) = $routes[$m];
            
                        foreach ($vars as $i => $v) {
                            if (isset($matches[1 + $i])) {
                                $ret[$v] = $matches[1 + $i];
                            }
                        }
            
                        $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                        if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                            if ($hasRequiredScheme) {
                                $allow += $requiredMethods;
                            }
                            break;
                        }
                        if (!$hasRequiredScheme) {
                            $allowSchemes += $requiredSchemes;
                            break;
                        }
            
                        return $ret;
                }

                if (127 === $m) {
                    break;
                }
                $regex = substr_replace($regex, 'F', $m - $offset, 1 + strlen($m));
                $offset += strlen($m);
            }
        }
        if ('/' === $pathinfo && !$allow && !$allowSchemes) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        return null;
    }
}
