<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXRIsXDa\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXRIsXDa/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerXRIsXDa.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerXRIsXDa\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \ContainerXRIsXDa\srcDevDebugProjectContainer(array(
    'container.build_hash' => 'XRIsXDa',
    'container.build_id' => 'dbfa3d87',
    'container.build_time' => 1541964749,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerXRIsXDa');
