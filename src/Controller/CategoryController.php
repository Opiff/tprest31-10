<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Category;

class CategoryController extends AbstractController
{
  /**
  * @Route("/category", name="category", methods="GET")
  */
  public function index(): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $categorys = $em->getRepository(Category::class)->findAll();

    $data = [];
    foreach ($categorys as $category) {
      array_push($data, [
        'id' => $category->getId(),
        'name' => $category->getName()
      ]);
    }

    $code = JsonResponse::HTTP_OK;
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/category/{id}", name="category_id", methods="GET")
  */
  public function category_id($id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();
    $categorys = $em->getRepository(Category::class)->findAll();

    $data = [];
    foreach ($categorys as $category) {
      if($category->getId() == $id)
      {
        array_push($data, [
          'id' => $category->getId(),
          'name' => $category->getName()
        ]);
        $code = JsonResponse::HTTP_OK;
      }
    }

    if ($data == [])
    {
      array_push($data, [
        'msg' => 'Not Found',
        'id' => $id
      ]);
      $code = JsonResponse::HTTP_NOT_FOUND;
    }

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/category", name="category_create", methods="POST")
  */
  public function category_create(Request $request): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();

    $input = json_decode($request->getContent(), true);

    $category = new Category();
    $category->setName($input['name']);

    $em->persist($category);
    $em->flush();

    $data = [
      'id' => $category->getId(),
      'name' => $category->getName()
    ];
    $code = JsonResponse::HTTP_CREATED;

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/category/{id}", name="category_modif", methods="PUT")
  */
  public function category_modif(Request $request, $id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();

    $input = json_decode($request->getContent(), true);

    $category = new Category();
    $category = $em->getRepository(Category::class)->findOneBy(['id' => $id]);
    $category->setName($input['name']);

    $em->persist($category);
    $em->flush();

    $data = [
      'id' => $category->getId(),
      'name' => $category->getName()
    ];
    $code = JsonResponse::HTTP_CREATED;

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

  /**
  * @Route("/category/{id}", name="category_delete", methods="DELETE")
  */
  public function category_delete($id): JsonResponse
  {
    $em = $this->getDoctrine()->getManager();

    $category = new Category();
    $category = $em->getRepository(Category::class)->findOneBy(['id' => $id]);

    if (!is_null($category)){
      $em->remove($category);
      $em->flush();
      $data = [];
      $code = JsonResponse::HTTP_NO_CONTENT;
    }

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    return new JsonResponse($json, $code, [], true);
  }

}
